
# Versión 0.6 - CHANGELOG #6

import pygame
import pygame.freetype
import random

FPS=60
WINDOW_SIZE_X=1200
WINDOW_SIZE_Y=860
PLAYER_VEL=0.5

# states 
STATE_USER=1
STATE_MENU=2
STATE_SUBMENU1=3
STATE_SUBMENU2=4
STATE_PLAYING_1=5
STATE_PLAYING_2=6
STATE_EXIT=7
STATE_GAMEOVER=8
STATE_NEXTLEVEL=9
STATE_GAMEWIN=10
# spawnsitos
SPAWN_POWERUP=5000
SPAWN_LASER_ENEMIE=500
SPAWN_ENEMIE=2000
#Move enemies
MOVE_ENEMIE=500
MOVE_BENEMIE=4000
def menu(screen,player,Naus):
    background=pygame.image.load('img/fondomenu2.png')
    titulo=pygame.image.load('img/titulo.png')
    EscogerNave=pygame.image.load('img/PNG/UI/EscogerNave.png')
    font = pygame.freetype.Font("fonts/static/SmoochSans-Black.ttf", 32)
    #Opciones Menu
    jugar=pygame.image.load('img/PNG/UI/Jugar.png')
    sortir=pygame.image.load('img/PNG/UI/Sortir.png')
    Volver=pygame.image.load('img/PNG/UI/Volver.png')
    #Naus
    NauBlue1Menu=pygame.image.load('img/PNG/PlayerNau1_blue_derecha.png')
    #Niveles
    Nivel1=pygame.image.load('img/PNG/UI/Nivel1.png')
    Nivel2=pygame.image.load('img/PNG/UI/Nivel2.png')
    Nivel2Bloq=pygame.image.load('img/PNG/UI/Nivel2Bloq.png')
    ########
    result=STATE_MENU
    going=True
    mostrar=False
    while going:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                if result==STATE_MENU:
                    square=jugar.get_rect().move(80,300)
                    if square.collidepoint(pygame.mouse.get_pos()):
                        result=STATE_SUBMENU1
                    square=sortir.get_rect().move(80,350)
                    if square.collidepoint(pygame.mouse.get_pos()):
                        going=False
                        result=STATE_EXIT
                elif result==STATE_SUBMENU1:
                    square=Volver.get_rect().move(80,300) 
                    if square.collidepoint(pygame.mouse.get_pos()):
                        result=STATE_MENU
                    square=Naus['NauBlue1'].get_rect().move(600-111,315)
                    if square.collidepoint(pygame.mouse.get_pos()):
                        player['NaveElegida']=Naus['NauBlue1']
                        result=STATE_SUBMENU2
                    square=Naus['NauRed2'].get_rect().move(710-111,315)
                    if square.collidepoint(pygame.mouse.get_pos()):
                        player['NaveElegida']=Naus['NauRed2']
                        result=STATE_SUBMENU2
                elif result==STATE_SUBMENU2:
                    square=Volver.get_rect().move(80,300) 
                    if square.collidepoint(pygame.mouse.get_pos()):
                        result=STATE_SUBMENU1
                    square=Nivel1.get_rect().move(600-111,300)
                    if square.collidepoint(pygame.mouse.get_pos()):
                        going=False
                        result=STATE_PLAYING_1
                    square=Nivel2Bloq.get_rect().move(600-111,350)
                    if square.collidepoint(pygame.mouse.get_pos()) and player['NextLevel']==False:
                        mostrar=True
                    square=Nivel2.get_rect().move(600-111,350)
                    if square.collidepoint(pygame.mouse.get_pos()) and player['NextLevel']==True:
                        going=False
                        result=STATE_PLAYING_2
        screen.blit(background, background.get_rect())
        screen.blit(titulo, titulo.get_rect().move(600-385,50))
        if result==STATE_MENU:
            screen.blit(jugar, jugar.get_rect().move(80,300))
            screen.blit(sortir, sortir.get_rect().move(80,350))
            screen.blit(NauBlue1Menu, NauBlue1Menu.get_rect().move(400,300))
        elif result==STATE_SUBMENU1:
            screen.blit(Volver, Volver.get_rect().move(80,300))
            screen.blit(EscogerNave, EscogerNave.get_rect().move(600-111,250))
            screen.blit(Naus['NauBlue1'], Naus['NauBlue1'].get_rect().move(600-111,315))
            screen.blit(Naus['NauRed2'], Naus['NauRed2'].get_rect().move(710-111,315))
        elif result==STATE_SUBMENU2:
            screen.blit(Volver, Volver.get_rect().move(80,300))           
            screen.blit(Nivel1, Nivel1.get_rect().move(600-111,300))
            if player['NextLevel']==False:
                screen.blit(Nivel2Bloq, Nivel2Bloq.get_rect().move(600-111,350))
            else:
                screen.blit(Nivel2, Nivel2.get_rect().move(600-111,350))
        if mostrar==True:
            draw_overlay(screen,font)
        pygame.display.flip()

    return result

def draw_overlay(screen, font):
    font.render_to(screen, (600-111, 400), 'Completa primero el Nivel 1')

def game_over(screen,player):
    result=STATE_GAMEOVER
    backgroundGO=pygame.image.load('img/Backgrounds/background_intro.png').convert()
    Game_Over=pygame.image.load('img/gameO.png')
    VolverMenu=pygame.image.load('img/PNG/UI/VolverMenu.png')
    gameover=True
    while gameover:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameover=False
                result=STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                if result==STATE_GAMEOVER:
                    square=VolverMenu.get_rect().move(600-111,720)
                    if square.collidepoint(pygame.mouse.get_pos()):
                        gameover=False
                        result=STATE_MENU
        screen.blit(backgroundGO, backgroundGO.get_rect())
        screen.blit(Game_Over, Game_Over.get_rect().move(600-325,50))
        screen.blit(player['NaveElegida'], player['NaveElegida'].get_rect().move(600-49,280))
        screen.blit(VolverMenu, VolverMenu.get_rect().move(600-111,720))
        pygame.display.flip()
    return result
def game_nextlevel(screen,player):
    result=STATE_NEXTLEVEL
    backgroundGO=pygame.image.load('img/Backgrounds/background_intro.png').convert()
    Next_Level=pygame.image.load('img/nextL.png')
    VolverMenu=pygame.image.load('img/PNG/UI/VolverMenu.png')
    NextLevel=pygame.image.load('img/PNG/UI/NextLevel.png')
    game_nextLevel=True
    while game_nextLevel:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_nextLevel=False
                result=STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                if result==STATE_NEXTLEVEL:
                    square=VolverMenu.get_rect().move(600-111,720)
                    if square.collidepoint(pygame.mouse.get_pos()):
                        game_nextLevel=False
                        result=STATE_MENU
                    square=NextLevel.get_rect().move(600-111,675)
                    if square.collidepoint(pygame.mouse.get_pos()):
                        game_nextLevel=False
                        result=STATE_PLAYING_2
        screen.blit(backgroundGO, backgroundGO.get_rect())
        screen.blit(Next_Level, Next_Level.get_rect().move(600-325,50))
        screen.blit(player['NaveElegida'], player['NaveElegida'].get_rect().move(600-49,280))
        screen.blit(VolverMenu, VolverMenu.get_rect().move(600-111,720))
        screen.blit(NextLevel, NextLevel.get_rect().move(600-111,675))
        pygame.display.flip()
    return result

def gamewin(screen,player):
    result=STATE_GAMEWIN
    backgroundGO=pygame.image.load('img/Backgrounds/background_intro.png').convert()
    win=pygame.image.load('img/Win.png')
    VolverMenu=pygame.image.load('img/PNG/UI/VolverMenu.png')
    gamewin=True
    while gamewin:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gamewin=False
                result=STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                if result==STATE_GAMEWIN:
                    square=VolverMenu.get_rect().move(600-111,720)
                    if square.collidepoint(pygame.mouse.get_pos()):
                        gamewin=False
                        result=STATE_MENU
        screen.blit(backgroundGO, backgroundGO.get_rect())
        screen.blit(win, win.get_rect().move(600-325,50))
        screen.blit(player['NaveElegida'], player['NaveElegida'].get_rect().move(600-49,280))
        screen.blit(VolverMenu, VolverMenu.get_rect().move(600-127,720))
        pygame.display.flip()
    return result

def empezar_partida_1(screen, player, Naus):
    result=STATE_PLAYING_1
    backgroundNivel1=pygame.image.load('img/Backgrounds/background_nivel1.png').convert()
    clock = pygame.time.Clock()
    laser_img=pygame.image.load('img/PNG/Lasers/laserGreen03.png')
    laserulti_img=pygame.image.load('img/PNG/Lasers/laserRed12.png')
    LifeNB1=pygame.image.load('img/PNG/UI/playerLife1_blue.png')
    LifeNR2=pygame.image.load('img/PNG/UI/playerLife2_red.png')
    laserenemie=pygame.image.load('img/PNG/Lasers/laserBlue13.png')
    volver=pygame.image.load('img/PNG/UI/Volver.png')
    explosion=pygame.image.load('img/Explosion.png')
    sound_explosion=pygame.mixer.Sound('Sounds/Explosion.mp3')
    ####Lasers####
    lasers=[]
    lasers_ultis=[]
    soundlaser=pygame.mixer.Sound('Sounds/sfx_laser1.ogg')
    soundlaser.set_volume(0.5)
    soundlaserulti=pygame.mixer.Sound('Sounds/laser-gun-19sf.mp3')
    ####PowerUps###
    powerup_types=create_powerups()
    powerups=[]
    ####Enemigos###
    enemigos=[]
    lasersEnList=[]
    soundlaserEn=pygame.mixer.Sound('Sounds/sfx_laser2.ogg')
    soundlaserEn.set_volume(0.5)
    ###############
    elapsed_time=0
    elapsed_timeME=0
    elapsed_timeE=0
    elapsed_timeBE=0
    partida=True
    contador_SE=0
    base_enemiga1={
        'sprite':pygame.image.load('img/PNG/ufoBlue.png'),
        'x':600,
        'y':-150,
        'life':5,
        'dead':False
    }
    while partida:
        delta=clock.tick(60)
        vel=int(PLAYER_VEL*delta)
        elapsed_time+=delta
        for enemigo in enemigos:
            enemigo['time']+=delta
            elapsed_timeME+=delta
        elapsed_timeE+=delta
        elapsed_timeBE+=delta
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                partida=False
                result=STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                square=volver.get_rect().move(975,790)
                if square.collidepoint(pygame.mouse.get_pos()):
                    partida=False
                    result=STATE_MENU
            if event.type == pygame.KEYDOWN:
                if event.key==pygame.K_1 and player['MuteSound']==False:
                    player['MuteSound']=True
                elif event.key==pygame.K_2 and player['MuteSound']==True:
                    player['MuteSound']=False
                if event.key==pygame.K_SPACE:
                    laser={
                        'laser_img':laser_img,
                        'x_laser':player['x'],
                        'y_laser':player['y'],
                        'time':10000
                        }
                    if len(lasers)!=2:
                        lasers.append(laser)
                        if player['MuteSound']!=True:
                            soundlaser.play()
                        
                if event.key == pygame.K_x:
                    laser_ulti={
                        'laserUlti_img':laserulti_img,
                        'x_ulti':player['x'],
                        'y_ulti':player['y'],
                        'time':10000
                    }
                    if player['Player_PowerUpR']!=False:
                        lasers_ultis.append(laser_ulti)
                        if player['MuteSound']!=True:
                            soundlaserulti.play()
                        player['Player_PowerUpR']=False
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and player['x']>0:
            player['x']=max(player['x']-vel, 0)
        if keys[pygame.K_RIGHT] and player['x']<WINDOW_SIZE_X-25:
            player['x']=min(player['x']+vel, WINDOW_SIZE_X-325)
        if keys[pygame.K_UP] and player['y']>0:
            player['y']=max(player['y']-vel, 0)
        if keys[pygame.K_DOWN] and player['y']<WINDOW_SIZE_Y-25:
            player['y']=min(player['y']+vel, WINDOW_SIZE_Y-65)
         
        ####Disparos####
        for n in range(len(lasers)-1, -1, -1):
            laser=lasers[n]
            laser['y_laser']-=int(0.6*delta)
            if laser['y_laser']<=0:
                del lasers[n]

        for n in range(len(lasers_ultis)-1, -1, -1):
            laser_ulti=lasers_ultis[n]
            laser_ulti['y_ulti']-=int(0.6*delta)
            if laser_ulti['y_ulti']<=0:
                del lasers_ultis[n]
        ################

        if elapsed_time>SPAWN_POWERUP:
            elapsed_time-=SPAWN_POWERUP
            powerups.append(spawn_powerups(powerup_types))
        
        for enemigo in enemigos:
            if enemigo['time']>SPAWN_LASER_ENEMIE:
                enemigo['time']-=random.randint(SPAWN_LASER_ENEMIE/2, 3*SPAWN_LASER_ENEMIE/2)
                laserEn={
                        'laserEn_img':laserenemie,
                        'x':enemigo['x'],
                        'y':enemigo['y']
                }
                if len(lasersEnList)!=1:
                    lasersEnList.append(laserEn)
                    if player['MuteSound']!=True:
                        soundlaserEn.play()

        
        if elapsed_timeE>SPAWN_ENEMIE:
            elapsed_timeE-=SPAWN_ENEMIE
            enemigo={
                'enemigo_img':pygame.image.load('img/PNG/Enemies/enemyBlue1.png'),
                'x':0,
                'y':0,
                'Life':3,
                'time':0,
                'dir':0
            }
            if contador_SE<14:
                enemigos.append(enemigo)
                contador_SE=contador_SE+1
            
        
        for n in range(len(lasersEnList)-1, -1, -1):
            laserEn=lasersEnList[n]
            laserEn['y']+=(0.6*delta)
            if laserEn['y']>=WINDOW_SIZE_Y:
                del lasersEnList[n]
            
        for n in range (len(enemigos)-1, -1, -1):
            enemigo=enemigos[n]
            if elapsed_timeME>=MOVE_ENEMIE:
                elapsed_timeME-=MOVE_ENEMIE
                enemigo['y']+=6
            if enemigo['dir']==0:
                enemigo['x']+=(0.1*delta)
            if enemigo['dir']==1:
                enemigo['x']-=(0.1*delta)

            if enemigo['x']>=830 and enemigo['dir']==0:
                enemigo['dir']=1
            if enemigo['x']<=0 and enemigo['dir']==1:
                enemigo['dir']=0

            if enemigo['Life']==0:
                del enemigos[n]
            if enemigo['y']>=WINDOW_SIZE_Y:
                del enemigos[n]

        if elapsed_timeBE>=MOVE_BENEMIE:
            elapsed_timeBE-=MOVE_BENEMIE
            base_enemiga1['y']+=12

        
        screen.blit(backgroundNivel1, backgroundNivel1.get_rect())
        screen.blit(player['NaveElegida'], player['NaveElegida'].get_rect().move(player['x'],player['y']))
        for enemigo in enemigos:
            screen.blit(enemigo['enemigo_img'], enemigo['enemigo_img'].get_rect().move(enemigo['x'],enemigo['y']))
        
        for laser in lasers:
            screen.blit(laser_img, laser_img.get_rect().move(laser['x_laser']+48,laser['y_laser']+37))
        
        for laserEn in lasersEnList:
            screen.blit(laserenemie, laserenemie.get_rect().move(laserEn['x']+48,laserEn['y']+37))

        for laser_ulti in lasers_ultis:
            screen.blit(laserulti_img, laserulti_img.get_rect().move(laser_ulti['x_ulti']+49,laser_ulti['y_ulti']-37))

        if player['NaveElegida']==Naus['NauBlue1']:
            for n in range(0,player['Life']):
                screen.blit(LifeNB1, LifeNB1.get_rect().move(975+n*45,490))
        elif player['NaveElegida']==Naus['NauRed2']:
            for n in range(0,player['Life']):
                screen.blit(LifeNR2, LifeNR2.get_rect().move(975+n*45,490))

        screen.blit(volver, volver.get_rect().move(975,790))
        if base_enemiga1['life']<=0:
            base_enemiga1['dead']=True
            screen.blit(explosion, explosion.get_rect().move(base_enemiga1['x'],base_enemiga1['y']))
            if player['MuteSound']!=True:
                sound_explosion.play()
        if base_enemiga1['dead']==False:
            screen.blit(base_enemiga1['sprite'], base_enemiga1['sprite'].get_rect().move(base_enemiga1['x'],base_enemiga1['y']))
        
        if player['Player_PowerUpE']!=False:
            powerup=player['Player_PowerUpE']
            screen.blit(powerup['sprite'], powerup['sprite'].get_rect().move(976,525))
        if player['Player_PowerUpR']!=False:
            powerup=player['Player_PowerUpR']
            screen.blit(powerup['sprite'], powerup['sprite'].get_rect().move(1020,525))
        
        if player['Life']==0:
            partida=False
            result=STATE_GAMEOVER
            player['Life']=3
            player['Player_PowerUpE']=False
            player['Player_PowerUpR']=False
        if base_enemiga1['dead']==True:
            partida=False
            result=STATE_NEXTLEVEL
            player['NextLevel']=True
            player['Life']=3
            player['Player_PowerUpE']=False
            player['Player_PowerUpR']=False
        
        powerups_colisions(player,powerups)
        powerups_decay(powerups,delta)
        dibujar_powerups(screen, powerups)
        laserEn_colisions(player,lasersEnList)
        laser_colisionsBE(base_enemiga1,lasers,lasers_ultis)
        for enemigo in enemigos:
            laser_colisions(enemigo,lasers,lasers_ultis)

        pygame.display.flip()
    
    return result

def empezar_partida_2(screen,player,Naus):
    result=STATE_PLAYING_1
    backgroundNivel2=pygame.image.load('img/Backgrounds/background_nivel2.png').convert()
    clock = pygame.time.Clock()
    laser_img=pygame.image.load('img/PNG/Lasers/laserGreen03.png')
    laserulti_img=pygame.image.load('img/PNG/Lasers/laserRed12.png')
    LifeNB1=pygame.image.load('img/PNG/UI/playerLife1_blue.png')
    LifeNR2=pygame.image.load('img/PNG/UI/playerLife2_red.png')
    laserenemie=pygame.image.load('img/PNG/Lasers/laserBlue13.png')
    volver=pygame.image.load('img/PNG/UI/Volver.png')
    explosion=pygame.image.load('img/Explosion.png')
    sound_explosion=pygame.mixer.Sound('Sounds/Explosion.mp3')
    ####Lasers####
    lasers=[]
    lasers_ultis=[]
    soundlaser=pygame.mixer.Sound('Sounds/sfx_laser1.ogg')
    soundlaser.set_volume(0.5)
    soundlaserulti=pygame.mixer.Sound('Sounds/laser-gun-19sf.mp3')
    ####PowerUps###
    powerup_types=create_powerups()
    powerups=[]
    ####Enemigos###
    enemigos=[]
    lasersEnList=[]
    soundlaserEn=pygame.mixer.Sound('Sounds/sfx_laser2.ogg')
    soundlaserEn.set_volume(0.5)
    ###############
    elapsed_time=0
    elapsed_timeME=0
    elapsed_timeE=0
    elapsed_timeBE=0
    partida=True
    base_enemiga2={
        'sprite':pygame.image.load('img/PNG/ufoRed.png'),
        'x':600,
        'y':-150,
        'life':8,
        'dead':False
    }
    while partida:
        delta=clock.tick(60)
        vel=int(PLAYER_VEL*delta)
        elapsed_time+=delta
        for enemigo in enemigos:
            enemigo['time']+=delta
            elapsed_timeME+=delta
        elapsed_timeE+=delta
        elapsed_timeBE+=delta
        print(base_enemiga2['dead'])
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                partida=False
                result=STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                square=volver.get_rect().move(975,790)
                if square.collidepoint(pygame.mouse.get_pos()):
                    partida=False
                    result=STATE_MENU
            if event.type == pygame.KEYDOWN:
                if event.key==pygame.K_1 and player['MuteSound']==False:
                    player['MuteSound']=True
                elif event.key==pygame.K_2 and player['MuteSound']==True:
                    player['MuteSound']=False
                if event.key==pygame.K_SPACE:
                    laser={
                        'laser_img':laser_img,
                        'x_laser':player['x'],
                        'y_laser':player['y'],
                        'time':10000
                        }
                    if len(lasers)!=2:
                        lasers.append(laser)
                        if player['MuteSound']!=True:
                            soundlaser.play()
                        
                if event.key == pygame.K_x:
                    laser_ulti={
                        'laserUlti_img':laserulti_img,
                        'x_ulti':player['x'],
                        'y_ulti':player['y'],
                        'time':10000
                    }
                    if player['Player_PowerUpR']!=False:
                        lasers_ultis.append(laser_ulti)
                        if player['MuteSound']!=True:
                            soundlaserulti.play()
                        player['Player_PowerUpR']=False
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and player['x']>0:
            player['x']=max(player['x']-vel, 0)
        if keys[pygame.K_RIGHT] and player['x']<WINDOW_SIZE_X-25:
            player['x']=min(player['x']+vel, WINDOW_SIZE_X-325)
        if keys[pygame.K_UP] and player['y']>0:
            player['y']=max(player['y']-vel, 0)
        if keys[pygame.K_DOWN] and player['y']<WINDOW_SIZE_Y-25:
            player['y']=min(player['y']+vel, WINDOW_SIZE_Y-65)
         
        ####Disparos####
        for n in range(len(lasers)-1, -1, -1):
            laser=lasers[n]
            laser['y_laser']-=int(0.6*delta)
            if laser['y_laser']<=0:
                del lasers[n]

        for n in range(len(lasers_ultis)-1, -1, -1):
            laser_ulti=lasers_ultis[n]
            laser_ulti['y_ulti']-=int(0.6*delta)
            if laser_ulti['y_ulti']<=0:
                del lasers_ultis[n]
        ################

        if elapsed_time>SPAWN_POWERUP:
            elapsed_time-=SPAWN_POWERUP
            powerups.append(spawn_powerups(powerup_types))
        
        for enemigo in enemigos:
            if enemigo['time']>SPAWN_LASER_ENEMIE:
                enemigo['time']-=random.randint(SPAWN_LASER_ENEMIE/2, 3*SPAWN_LASER_ENEMIE/2)
                laserEn={
                        'laserEn_img':laserenemie,
                        'x':enemigo['x'],
                        'y':enemigo['y']
                }
                if len(lasersEnList)!=2:
                    lasersEnList.append(laserEn)
                    if player['MuteSound']!=True:
                        soundlaserEn.play()

        
        if elapsed_timeE>SPAWN_ENEMIE:
            elapsed_timeE-=SPAWN_ENEMIE
            enemigo={
                'enemigo_img':pygame.image.load('img/PNG/Enemies/enemyRed3.png'),
                'x':0,
                'y':0,
                'Life':3,
                'time':0,
                'dir':0
            }
            enemigos.append(enemigo)

            
        
        for n in range(len(lasersEnList)-1, -1, -1):
            laserEn=lasersEnList[n]
            laserEn['y']+=(0.6*delta)
            if laserEn['y']>=WINDOW_SIZE_Y:
                del lasersEnList[n]
            
        for n in range (len(enemigos)-1, -1, -1):
            enemigo=enemigos[n]
            if elapsed_timeME>=MOVE_ENEMIE:
                elapsed_timeME-=MOVE_ENEMIE
                enemigo['y']+=8
            if enemigo['dir']==0:
                enemigo['x']+=(0.1*delta)
            if enemigo['dir']==1:
                enemigo['x']-=(0.1*delta)

            if enemigo['x']>=830 and enemigo['dir']==0:
                enemigo['dir']=1
            if enemigo['x']<=0 and enemigo['dir']==1:
                enemigo['dir']=0

            if enemigo['Life']==0:
                del enemigos[n]
            if enemigo['y']>=WINDOW_SIZE_Y:
                del enemigos[n]

        if elapsed_timeBE>=MOVE_BENEMIE:
            elapsed_timeBE-=MOVE_BENEMIE
            base_enemiga2['y']+=8

        
        screen.blit(backgroundNivel2, backgroundNivel2.get_rect())
        screen.blit(player['NaveElegida'], player['NaveElegida'].get_rect().move(player['x'],player['y']))
        for enemigo in enemigos:
            screen.blit(enemigo['enemigo_img'], enemigo['enemigo_img'].get_rect().move(enemigo['x'],enemigo['y']))
        
        for laser in lasers:
            screen.blit(laser_img, laser_img.get_rect().move(laser['x_laser']+48,laser['y_laser']+37))
        
        for laserEn in lasersEnList:
            screen.blit(laserenemie, laserenemie.get_rect().move(laserEn['x']+48,laserEn['y']+37))

        for laser_ulti in lasers_ultis:
            screen.blit(laserulti_img, laserulti_img.get_rect().move(laser_ulti['x_ulti']+49,laser_ulti['y_ulti']-37))

        if player['NaveElegida']==Naus['NauBlue1']:
            for n in range(0,player['Life']):
                screen.blit(LifeNB1, LifeNB1.get_rect().move(975+n*45,490))
        elif player['NaveElegida']==Naus['NauRed2']:
            for n in range(0,player['Life']):
                screen.blit(LifeNR2, LifeNR2.get_rect().move(975+n*45,490))

        screen.blit(volver, volver.get_rect().move(975,790))
        if base_enemiga2['life']<=0:
            base_enemiga2['dead']=True
            screen.blit(explosion, explosion.get_rect().move(base_enemiga2['x'],base_enemiga2['y']))
            if player['MuteSound']!=True:
                sound_explosion.play()
        if base_enemiga2['dead']==False:
            screen.blit(base_enemiga2['sprite'], base_enemiga2['sprite'].get_rect().move(base_enemiga2['x'],base_enemiga2['y']))
        
        if player['Player_PowerUpE']!=False:
            powerup=player['Player_PowerUpE']
            screen.blit(powerup['sprite'], powerup['sprite'].get_rect().move(976,525))
        if player['Player_PowerUpR']!=False:
            powerup=player['Player_PowerUpR']
            screen.blit(powerup['sprite'], powerup['sprite'].get_rect().move(1020,525))
        
        if player['Life']==0:
            partida=False
            result=STATE_GAMEOVER
            player['Life']=3
            player['Player_PowerUpE']=False
            player['Player_PowerUpR']=False
        if base_enemiga2['dead']==True:
            partida=False
            result=STATE_GAMEWIN
            player['NextLevel']=True
            player['Player_PowerUpE']=False
            player['Player_PowerUpR']=False

        print(player['MuteSound'])
        powerups_colisions(player,powerups)
        powerups_decay(powerups,delta)
        dibujar_powerups(screen, powerups)
        laserEn_colisions(player,lasersEnList)
        laser_colisionsBE(base_enemiga2,lasers,lasers_ultis)
        for enemigo in enemigos:
            laser_colisions(enemigo,lasers,lasers_ultis)

        pygame.display.flip()
    
    return result
def laser_colisionsBE(base_enemiga1,lasers,lasers_ultis):
    sprite=base_enemiga1['sprite']
    square=sprite.get_rect().move(base_enemiga1['x'],base_enemiga1['y'])
    for laser in lasers:
        laser_square=laser['laser_img'].get_rect().move(laser['x_laser'],laser['y_laser'])
        if square.colliderect(laser_square):
            base_enemiga1['life']-=1
            laser['y_laser']=0
    for laser_ulti in lasers_ultis:
        laserulti_square=laser_ulti['laserUlti_img'].get_rect().move(laser_ulti['x_ulti'],laser_ulti['y_ulti'])
        if square.colliderect(laserulti_square):
            base_enemiga1['life']-=3
            laser_ulti['y_ulti']=0

def laser_colisions(enemigo,lasers,lasers_ultis):
    sprite=enemigo['enemigo_img']
    square=sprite.get_rect().move(enemigo['x'],enemigo['y'])
    for laser in lasers:
        laser_square=laser['laser_img'].get_rect().move(laser['x_laser'],laser['y_laser'])
        if square.colliderect(laser_square):
            enemigo['Life']-=1
            laser['y_laser']=0
    for laser_ulti in lasers_ultis:
        laserulti_square=laser_ulti['laserUlti_img'].get_rect().move(laser_ulti['x_ulti'],laser_ulti['y_ulti'])
        if square.colliderect(laserulti_square):
            enemigo['Life']-=3
            laser_ulti['y_ulti']=0
        
def laserEn_colisions(player,lasersEnList):
    PowerUpNo=pygame.mixer.Sound('Sounds/sfx_shieldDown.ogg')
    PowerUpNo.set_volume(1.0)
    sprite=player['NaveElegida']
    square=sprite.get_rect().move(player['x'], player['y'])
    for laserEn in lasersEnList:
        laserEn_square=laserEn['laserEn_img'].get_rect().move(laserEn['x'], laserEn['y'])
        if square.colliderect(laserEn_square):
            if player['Player_PowerUpE']==False:
                player['Life']-=1
            if player['Player_PowerUpE']!=False:
                player['Player_PowerUpE']=False
                if player['MuteSound']!=True:
                    PowerUpNo.play()

            laserEn['y']=WINDOW_SIZE_Y
def powerups_colisions(player,powerups):
    PowerUpYes=pygame.mixer.Sound('Sounds/sfx_shieldUp.ogg')
    PowerUpYes.set_volume(1.0)
    sprite=player['NaveElegida']
    square=sprite.get_rect().move(player['x'], player['y'])
    if player['Player_PowerUpE']==False:
        for powerup in powerups:
            powerup_square=powerup['sprite'].get_rect().move(powerup['x'], powerup['y'])
            if square.colliderect(powerup_square) and powerup['name']=='Escudo':
                player['Player_PowerUpE']=powerup
                powerup['time']=0
                if player['MuteSound']!=True:
                    PowerUpYes.play()
                
    if player['Player_PowerUpR']==False:
        for powerup in powerups:
            powerup_square=powerup['sprite'].get_rect().move(powerup['x'], powerup['y'])
            if square.colliderect(powerup_square) and powerup['name']=='Rayo':
                player['Player_PowerUpR']=powerup
                powerup['time']=0
                if player['MuteSound']!=True:
                    PowerUpYes.play()

def powerups_decay(powerups, delta):
    for n in range(len(powerups)-1, -1, -1):
        powerup=powerups[n]
        powerup['time']-=delta
        if powerup['time']<=0:
            del powerups[n]

def spawn_powerups(powerups_types):
    powerup_types=random.choice(powerups_types)
    powerup={
        'sprite': powerup_types['sprite'],
        'time': powerup_types['time'],
        'life': powerup_types['life'],
        'name':powerup_types['name'],
        'x': random.randint(0, 865-powerup_types['sprite'].get_width()),
        'y': random.randint(0, WINDOW_SIZE_Y-powerup_types['sprite'].get_height())
    }

    return powerup

def dibujar_powerups(screen, powerups):
    for powerup in powerups:
        sprite=powerup['sprite']
        square=sprite.get_rect().move(powerup['x'], powerup['y'])
        screen.blit(sprite, square)

def create_powerups():
    powerups_types=[
        {'sprite': pygame.image.load('img/PNG/Power-ups/shield_silver.png'),
         'time': 12000,
         'life': 15,
         'name':'Escudo'},
        {'sprite': pygame.image.load('img/PNG/Power-ups/bolt_gold.png'),
         'time': 12000,
         'life':0,
         'name':'Rayo'
         }
    ]

    return powerups_types

def main():
    pygame.init()
    screen = pygame.display.set_mode([WINDOW_SIZE_X, WINDOW_SIZE_Y])
    game_icon = pygame.image.load('img/PNG/UI/playerLife1_blue.png')
    Naus={
        'NauBlue1':pygame.image.load('img/PNG/PlayerNau1_blue.png'),
        'NauRed2':pygame.image.load('img/PNG/PlayerNau2_Red.png')
    }
    player={'NaveElegida':'',
            'x':600-111,
            'y':650,
            'Player_PowerUpE':False,
            'Player_PowerUpR':False,
            'Life':3,
            'NextLevel':False,
            'MuteSound':False
    }


    pygame.display.set_caption('Space Invaders')
    pygame.display.set_icon(game_icon)

    state=STATE_MENU
    while state!=STATE_EXIT:
        if state==STATE_MENU:
            state=menu(screen, player,Naus)
        elif state==STATE_PLAYING_1:
            state=empezar_partida_1(screen, player,Naus)
        elif state==STATE_PLAYING_2:
            state=empezar_partida_2(screen,player,Naus)
        elif state==STATE_GAMEOVER:
            state=game_over(screen,player)
        elif state==STATE_NEXTLEVEL:
            state=game_nextlevel(screen,player)
        elif state==STATE_GAMEWIN:
            state=gamewin(screen,player)
    pygame.quit()

main()